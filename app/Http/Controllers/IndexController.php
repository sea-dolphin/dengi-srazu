<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    /**
     * Views control for auth users
     * 
     * @return view
     */
    public function index()
    {
        if ( Auth::check() )
        {
            return view('index');
        }
        else
        {
            //Login page
            return view('layouts.app');
        }
    }
}
