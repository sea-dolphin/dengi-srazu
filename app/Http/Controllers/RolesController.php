<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Role;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oRoles = Role::all();
        
        return view('roles.roles', [ 'oRoles' => $oRoles ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.role_edit', [ 'oData' => null ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $oData = Role::where('id', $id)->first();
        
        return view('roles.role_edit', [ 'oData' => $oData ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ( !empty($request->input('name')) )
        {
            $aData = [
                'name' => $request->input('name'),
                'create_item' => empty($request->input('create_item')) ? '' : $request->input('create_item'),
                'edit_item' => empty($request->input('edit_item')) ? '' : $request->input('edit_item'),
                'delete_item' => empty($request->input('delete_item')) ? '' : $request->input('delete_item'),
                'sql_read' => empty($request->input('sql_read')) ? '' : $request->input('sql_read'),
                'sql_read_param' => empty($request->input('sql_read_param')) ? '' : $request->input('sql_read_param'),
                'sql_edit' => empty($request->input('sql_edit')) ? '' : $request->input('sql_edit'),
                'sql_edit_param' => empty($request->input('sql_edit_param')) ? '' : $request->input('sql_edit_param'),
            ];
            
            Role::where('id', $id)->update($aData);
        }
        
        return redirect('/admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
