<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Role;
use App\Models\Department;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$t = DB::select("SELECT * FROM departments WHERE type_item = 'type_2' ");
        //dd($t);
        $oCurrentRole = Role::where('id', Auth::user()->role_id )->first();
        
        if ( !empty($oCurrentRole->sql_read) )
        {
            $aDepartments = DB::select($oCurrentRole->sql_read);
        }
        else
        {
            $aDepartments = Department::all();
        }
        
        return view('departments.departments', [ 'aDepartments' => $aDepartments ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $oCurrentRole = Role::where('id', Auth::user()->role_id )->first();
        
        if ( $oCurrentRole->create_item == 1 )
        {
            return view('departments.department_edit', [ 'oData' => null ] );
        }
        else
        {
            return view('errors.permission');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $oCurrentRole = Role::where('id', Auth::user()->role_id )->first();
        $oDepartment = Department::where('id', $id)->first();
        $aIdRolePermission = [];
        
        if ( !empty($oCurrentRole->sql_edit) )
        {
            $strSqlEdit = str_replace('{current_user_id}',  Auth::user()->id, $oCurrentRole->sql_edit);
            
            $aRolePermission = DB::select($strSqlEdit);
            
            foreach ($aRolePermission as $oVal)
            {
                $aIdRolePermission[] = $oVal->id;
            }
            
            if ( !in_array($oDepartment->id, $aIdRolePermission) )
            {
                return view('errors.permission');
            }
        }
        
        if ( $oCurrentRole->edit_item != 1 )
        {
            return view('errors.permission');
        }
        
        return view('departments.department_edit', [ 'oData' => $oDepartment ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
