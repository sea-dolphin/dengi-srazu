<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ 'as' => 'index', 'uses' => 'IndexController@index' ]);
Route::get('/logout', [ 'as' => 'index', 'uses' => 'Auth\LoginController@logout' ]);

Route::get('/test', [ 'as' => 'test', 'uses' => 'TestController@index' ]);

Auth::routes();

Route::group([ 'prefix' => 'admin', 'middleware' => 'auth' ], function() {
    Route::resource('roles', 'RolesController');
    Route::resource('users', 'UsersController');
});

Route::resource('departments', 'DepartmentsController');
