myApp.controller('BackendController', 
    function BackendController($scope, $http) {
        
        $scope.bigData = {};
        $scope.showLoader = false;
        
        $scope.loadBigData = function () {
            $scope.showLoader = true;
            
            $http.post('/loadBigData')
                .then(function (response) {
                    $scope.bigData = response.data.data;
                    $scope.showLoader = false;
                });
        };
        
        $scope.getGraphData = function () {
            $scope.showLoader = true;
            
            $http.post('/getGraphData')
                .then(function (response) {
                    console.log(response.data.data);
                    $scope.data = response.data.data;
                    $scope.showLoader = false;
                });
        };
        
        //Library: http://jtblin.github.io/angular-chart.js/
        
        $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
        $scope.series = ['Series A', 'Series B'];
        $scope.getGraphData();
        
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
        $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
        $scope.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: true,
                        position: 'right'
                    }
                ]
            }
        };
  
    }
)

