myApp.controller('SecurityController', 
    function SecurityController($scope, $mdDialog) {
      
        $scope.items = [ 'No Timeout', '1 Minute', '2 Minutes', '5 Minutes', '10 Minutes', '1 Hour', '3 Hours', '5 Hours', '10 Hours', '24 Hours' ];
        $scope.shell_timeout = 'No Timeout';
        
        $scope.showConfirm = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
              .title('Are you sure?')
              .textContent('After the device is rebooted, all changes will take effect.')
              .ariaLabel('Lucky day')
              .targetEvent(ev)
              .ok('Ok')
              .cancel('Cancel');

        $mdDialog.show(confirm).then(function() {
            //
        }, function() {
            //
        });
      };
    }
)

