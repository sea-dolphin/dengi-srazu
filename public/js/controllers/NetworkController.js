myApp.controller('NetworkController', 
    function NetworkController($scope, $mdDialog, $http) {
        
        $scope.ipv4 = {
            ip: ''
        };
        $scope.ipv4_2 = {};
        
        $scope.getIp = function () {
            $http.post('/getIp', {
                ip: '172.16.100.100'
            })
            .then(function (response) {
                $scope.ipv4.ip = response.data.data.ip;
            });
        };
        
        $scope.setIp = function () {
            console.log('Click set IP');
            
            $http.post('/setIp', {
                ip: $scope.ipv4.ip
            })
            .then(function (response) {
                console.log(response.data);
                $scope.showMessage(response.data.data.message_title, response.data.data.message_content);
            });
        };
      
        $scope.showAlert = function(ev) {
            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title('Warning')
                    .textContent('You can not add an item.')
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Ok')
                    .targetEvent(ev)
            );
        };
        
        $scope.showMessage = function (title, content) {
            $mdDialog.show(
                $mdDialog.alert()
                    .clickOutsideToClose(true)
                    .title(title)
                    .textContent(content)
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Ok')
            );
        };
        
        $scope.getIp();
    }
)

