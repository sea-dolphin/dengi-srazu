<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123'),
            'role_id' => 1,
        ]);
        
        DB::table('users')->insert([
            'name' => 'User1',
            'email' => 'user1@gmail.com',
            'password' => bcrypt('123'),
            'role_id' => 2,
        ]);
        
        DB::table('users')->insert([
            'name' => 'User2',
            'email' => 'user2@gmail.com',
            'password' => bcrypt('123'),
            'role_id' => 3,
        ]);
        
        DB::table('users')->insert([
            'name' => 'User3',
            'email' => 'user3@gmail.com',
            'password' => bcrypt('123'),
            'role_id' => 4,
        ]);
        
        DB::table('users')->insert([
            'name' => 'User4',
            'email' => 'user4@gmail.com',
            'password' => bcrypt('123'),
            'role_id' => 5,
        ]);
        
        //-----------------------
        
        DB::table('departments')->insert([
            'name' => 'Ростовский',
            'type_item' => 'type_1',
            'active' => 1,
        ]);
        
        DB::table('departments')->insert([
            'name' => 'Московский',
            'type_item' => 'type_2',
            'active' => 1,
        ]);
        
        DB::table('departments')->insert([
            'name' => 'Воронежский',
            'type_item' => 'type_3',
            'active' => 1,
        ]);
        
        DB::table('departments')->insert([
            'name' => 'Краснодарский',
            'type_item' => 'type_4',
            'active' => 1,
        ]);
        
        DB::table('departments')->insert([
            'name' => 'Сочинский',
            'type_item' => 'type_5',
            'active' => 1,
        ]);
        
        //---------------------
        
        DB::table('user_departments')->insert([
            'user_id' => '1',
            'department_id' => '1',
        ]);
        
        DB::table('user_departments')->insert([
            'user_id' => '2',
            'department_id' => '2',
        ]);
        
        DB::table('user_departments')->insert([
            'user_id' => '3',
            'department_id' => '3',
        ]);
        
        DB::table('user_departments')->insert([
            'user_id' => '4',
            'department_id' => '4',
        ]);
        
        DB::table('user_departments')->insert([
            'user_id' => '5',
            'department_id' => '5',
        ]);
        
        //-------------------------
        
        DB::table('roles')->insert([
            'name' => 'Полные права',
            'read_item' => 1,
            'create_item' => 1,
            'edit_item' => 1,
            'delete_item' => 1,
        ]);
        
        DB::table('roles')->insert([
            'name' => 'Показ только определённого типа',
            'read_item' => 1,
            'create_item' => 1,
            'edit_item' => 1,
            'delete_item' => 1,
        ]);
        
        DB::table('roles')->insert([
            'name' => 'Только активных и кроме определенного типа',
            'read_item' => 1,
            'create_item' => 1,
            'edit_item' => 1,
            'delete_item' => 1,
        ]);
        
        DB::table('roles')->insert([
            'name' => 'Просматривать все подразделения, но редактировать только свои',
            'read_item' => 1,
            'create_item' => 1,
            'edit_item' => 1,
            'delete_item' => 1,
        ]);
        
        DB::table('roles')->insert([
            'name' => 'Составное и сложное условие',
            'read_item' => 1,
            'create_item' => 1,
            'edit_item' => 1,
            'delete_item' => 1,
        ]);
    }
}
