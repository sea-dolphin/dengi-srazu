@extends('index')

@section('content')

    <h2 class="sub-header">Редактирование информации о департаменте</h2>

    <div class="list list-group" style="margin-top: 80px">
        <form method="post" action="/departments{{ empty($oData->id) ? '' : '/'.$oData->id }}">
            @if ( !empty($oData->id) )
                {{ method_field('PUT') }}
            @endif
            <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
            
            <div class="form-group">
                <label for="roleName">Название</label>
                <input type="text" name="name" class="form-control" id="roleName" placeholder="Название" required="" value="{{ empty($oData->name) ? '' : $oData->name }}">
            </div>
            <div class="form-group">
                <label for="createItem">Тип</label>
                <input type="text" name="type_item" class="form-control" id="createItem" placeholder="тип" required="" value="{{ empty($oData->type_item) ? '' : $oData->type_item }}">
            </div>
            <div class="form-group">
                <label for="editItem">Активность</label>
                <input type="text" name="active" class="form-control" id="editItem" placeholder="Активность" required="" value="{{ empty($oData->active) ? '' : $oData->active }}">
            </div>
            
            <button type="submit" class="btn btn-primary">
                @if ( empty($oData->id) )
                    Создать
                @else
                    Сохранить
                @endif
            </button>
        </form>
    </div>

@endsection