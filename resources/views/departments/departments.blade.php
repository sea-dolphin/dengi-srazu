@extends('index')

@section('content')

<h2 class="sub-header">Департаменты</h2>

<!--<a href="/departments/create" class="btn btn-primary">Добавить департамент</a>-->

<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Название</th>
        <th>Тип</th>
        <th>Активный</th>
        <th>Редактировать</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($aDepartments as $oDepartment)
            <tr>
                <td>{{ $oDepartment->id }}</td>
                <td>{{ $oDepartment->name }}</td>
                <td>{{ $oDepartment->type_item }}</td>
                <td>
                    @if ( $oDepartment->active == 1 )
                        Да
                    @else
                        Нет
                    @endif
                </td>
                <td><a href="/departments/{{ $oDepartment->id }}/edit" title="edit"><span class="glyphicon glyphicon-pencil"></span></a></td>
            </tr>
        @endforeach
    </tbody>
  </table>
</div>

@endsection

