@extends('index')

@section('content')

    <h2 class="sub-header">Редактирование роли</h2>

    <div class="list list-group" style="margin-top: 80px">
        <form method="post" action="/admin/roles{{ empty($oData->id) ? '' : '/'.$oData->id }}">
            @if ( !empty($oData->id) )
                {{ method_field('PUT') }}
            @endif
            <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
            
            <div class="form-group">
                <label for="roleName">Название роли</label>
                <input type="text" name="name" class="form-control" id="roleName" placeholder="Название роли" required="" value="{{ empty($oData->name) ? '' : $oData->name }}">
            </div>
            <div class="form-group">
                <label for="createItem">Создание элемента</label>
                <input type="text" name="create_item" class="form-control" id="createItem" placeholder="Создание элемента" required="" value="{{ empty($oData->create_item) ? '' : $oData->create_item }}">
            </div>
            <div class="form-group">
                <label for="editItem">Редактирование элемента</label>
                <input type="text" name="edit_item" class="form-control" id="editItem" placeholder="Редактирование элемента" required="" value="{{ empty($oData->edit_item) ? '' : $oData->edit_item }}">
            </div>
            <div class="form-group">
                <label for="deleteItem">Удаление элемента</label>
                <input type="text" name="delete_item" class="form-control" id="deleteItem" placeholder="Удаление элемента" required="" value="{{ empty($oData->delete_item) ? '' : $oData->delete_item }}">
            </div>
            
            <div class="form-group">
                <label for="comment">SQL на чтение списка (Параметры указывать в одинарных кавычках):</label>
                <textarea class="form-control" rows="5" id="comment" name="sql_read">{{ empty($oData->sql_read) ? '' : $oData->sql_read }}</textarea>
            </div>
            
            <div class="form-group">
                <label for="comment">SQL на редактирование (Параметры указывать в одинарных кавычках):</label>
                <textarea class="form-control" rows="5" id="comment" name="sql_edit">{{ empty($oData->sql_edit) ? '' : $oData->sql_edit }}</textarea>
            </div>
<!--            <div class="col-xs-6 form-group">
                <label for="comment">Параметры для SQL запроса на чтение. Писать через запятую без пробелов:</label>
                <textarea class="form-control" rows="5" id="comment" name="sql_read_param"></textarea>
            </div>-->
            <button type="submit" class="btn btn-primary">
                @if ( empty($oData->id) )
                    Создать роль
                @else
                    Сохранить
                @endif
            </button>
        </form>
    </div>

@endsection