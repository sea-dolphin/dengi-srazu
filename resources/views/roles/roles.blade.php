@extends('index')

@section('content')

<h2 class="sub-header">Роли пользователей</h2>

<a href="/admin/roles/create" class="btn btn-primary">Добавить роль</a>

<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Название</th>
        <th>Создание</th>
        <th>Ред.</th>
        <th>Удал.</th>
        <th>SQL на чтение</th>
        <th>SQL на ред.</th>
        <th>Изменить роль</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($oRoles as $oRole)
            <tr>
                <td>{{ $oRole->id }}</td>
                <td>{{ $oRole->name }}</td>
                <td>
                    @if ( $oRole->create_item == 1 )
                        Да
                    @else
                        Нет
                    @endif
                </td>
                <td>
                    @if ( $oRole->edit_item == 1 )
                        Да
                    @else
                        Нет
                    @endif
                </td>
                <td>
                    @if ( $oRole->delete_item == 1 )
                        Да
                    @else
                        Нет
                    @endif
                </td>
                <td>{{ $oRole->sql_read }}</td>
                <td>{{ $oRole->sql_edit }}</td>
                <td><a href="/admin/roles/{{ $oRole->id }}/edit" title="edit"><span class="glyphicon glyphicon-pencil"></span></a></td>
            </tr>
        @endforeach
    </tbody>
  </table>
</div>

@endsection

