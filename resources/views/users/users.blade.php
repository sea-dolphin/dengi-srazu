@extends('index')

@section('content')

<h2 class="sub-header">Пользователи</h2>

<!--<a href="/admin/users/create" class="btn btn-primary">Добавить пользователя</a>-->

<div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Имя</th>
        <th>Email</th>
        <th>Роль</th>
        <th>Редактировать</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($aData as $oData)
            <tr>
                <td>{{ $oData->id }}</td>
                <td>{{ $oData->name }}</td>
                <td>{{ $oData->email }}</td>
                <td>{{ $oData->role->name }}</td>
                <td><a href="#" title="edit"><span class="glyphicon glyphicon-pencil"></span></a></td>
            </tr>
        @endforeach
    </tbody>
  </table>
</div>

@endsection

