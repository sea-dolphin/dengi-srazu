<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/assets/images/favicon.ico">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Деньги сразу. Тестовое задание</title>
    
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
<!--    <script src="/js/_app.js"></script>-->

    <!-- Bootstrap core CSS -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    
    <link href="/css/login.css" rel="stylesheet">
    
    
    <!-- Angular Material style sheet -->
<!--    <link rel="stylesheet" href="/assets/css/angular-material.min.css">
    
    <base href="/">-->

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body ng-app="myApp">

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid"></div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            @if ( Auth::check() )
                <ul class="nav nav-sidebar">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/departments">Департаменты</a></li>
                    @if ( Auth::user()->name == 'Admin' )
                        <li><a href="/admin/roles">Роли</a></li>
                        <li><a href="/admin/users">Пользователи</a></li>
                    @endif
                    <li><a href="/logout">Выход</a></li>
                </ul>
            @endif
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            
            @yield('content')
            
<!--            <div ng-view class="top_padding_content"></div>-->
          
        </div>
      </div>
    </div>
      
    
<!--    <script src="/assets/js/angularjs/angular.min.js"></script>
    <script src="/assets/js/angularjs/angular-route.min.js"></script>
    <script src="/assets/js/angularjs/ui-bootstrap-2.5.0.min.js"></script>
    
     Angular Material Library 
    <script src="/assets/js/angularjs/angular-animate.min.js"></script>
    <script src="/assets/js/angularjs/angular-aria.min.js"></script>
    <script src="/assets/js/angularjs/angular-material.min.js"></script>
    <script src="/assets/js/angularjs/angular-messages.min.js"></script>-->
    
<!--    <script src="/js/app.js"></script>
    <script src="/js/controllers/MainController.js"></script>
    <script src="/js/controllers/NetworkController.js"></script>
    <script src="/js/controllers/SecurityController.js"></script>
    <script src="/js/controllers/NtpController.js"></script>
    <script src="/js/controllers/BackendController.js"></script>-->
  </body>
</html>

